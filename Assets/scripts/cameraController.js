﻿#pragma strict

private var startTouch : Vector2;
private var startCameraPos : Vector3;

private var touchPinch1 : Vector2;
private var touchPinch2 : Vector2;
private var startCameraPinch : Vector3;

private var isDragging : boolean;

function Start () {

}

function Update () {
	if(Input.touchCount == 1)
	{
		// Drag camera
		if(Input.touches[0].phase == TouchPhase.Began)
		{
			isDragging = true;
			startCameraPos = transform.position;
			
			startTouch = Input.touches[0].position;
		} else {
			if(isDragging)
			{
				transform.position.x = startCameraPos.x - (Input.touches[0].position.x - startTouch.x)/100;
				transform.position.z = startCameraPos.z - (Input.touches[0].position.y - startTouch.y)/100;			
			}
		}
	}


	// Pinch
	if(Input.touchCount == 2)
	{
		if(Input.touches[1].phase == TouchPhase.Began)
		{
			isDragging = false;
			
			touchPinch1 = Input.touches[0].position;
			touchPinch2 = Input.touches[1].position;
			
			startCameraPinch = transform.position;
		} else {
			var startDistance = Vector2.Distance(touchPinch1, touchPinch2);
			var nowDistance = Vector2.Distance(Input.touches[0].position, Input.touches[1].position);
			
			var newPosition = startCameraPinch + transform.forward * (nowDistance - startDistance)/10;
			
			if(newPosition.y > 1 && newPosition.y < 11)
			{
				transform.position = newPosition;
			}
		}
	}
	
	
	if(Input.GetKey("w"))
	{
		transform.position = transform.position + transform.forward;
	}
	if(Input.GetKey("s"))
	{
		transform.position = transform.position - transform.forward;
	}
}